package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.api.Assertions.*;

class StringsUtilTest {

    @ParameterizedTest
    //given
    @CsvSource(value = {"test;TEST", "   jaVa  ;JAVA", "teST ;TEST"}, delimiter = ';')
    public void shouldTrimAndUpperCaseInput(String input, String expected) {
        //when
        String actual = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, actual);
    }


    @ParameterizedTest
    //given
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1, delimiter = ',', lineSeparator = ";")
    public void shouldTrimAndUpperCaseInputCSVFile(String input, String expected) {
        //when
        String actual = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @NullSource
    public void shoudBeBlankForNull(String input) {
        assertTrue(StringsUtil.isBlank(input));
    }

    @ParameterizedTest
    @EmptySource
    public void shoudBeBlankForEmptyString(String input) {
        assertTrue(StringsUtil.isBlank(input));
    }

    //TODO
    //Praca domowa
    //Zaimplementować przypadki negatywne dla metod toUpperCase, isBlank

    //poniższy test jest równoważny dwóm powyższym testom

    @ParameterizedTest
    @NullAndEmptySource
    public void shoudBeBlankForNullOrEmptyString(String input) {
        assertTrue(StringsUtil.isBlank(input));
    }

}